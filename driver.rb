require 'tax_calculator'

calc = TaxCalculator.new({
  :income => 200000,
  :no_of_children => 1,
  :blind => false,
  :student => false,
  :military => false
  })

# calc.calc_tax
# puts calc.income_tax.to_s('10F')
# puts calc.social_security_tax.to_s('10F')

puts "Income: $#{calc.income}"
puts "Income Tax rate: #{calc.rate}%"
puts "Social Security Tax rate: #{calc.ss_rate}%"
puts "Number of children: #{calc.no_of_children}"
puts "Income tax amount: $#{calc.income_tax}"
puts "Social Security tax amount: $#{calc.social_security_tax}"
