require 'bigdecimal'
require 'bigdecimal/util'

class TaxCalculator
  # attr_accessor(:income, :no_of_children, :blind, :military, :student)
  # attr_reader(:income_tax, :social_security_tax)

  def initialize(args)
    if args.is_a? Hash
      @income = args.fetch(:income, 0)  # initialize with a default of 0
      @no_of_children = args.fetch(:no_of_children, 0)  # initialize with a default of 0
      @blind = args.fetch(:blind, false)  # initialize with a default of false
      @military = args.fetch(:military, false)  # initialize with a default of false
      @student = args.fetch(:student, false)  # initialize with a default of false

      if @income < 0
        raise "Invalid income"
      end
      if @no_of_children < 0
        raise "Invalid number of children"
      end
      self.calc_tax
    end
  end

  # Calculates tax and sets @income_tax and @social_security_tax
  def calc_tax
    @ss_rate = 0.065.to_d
    # apply 6.5% social security tax first
    @social_security_tax = @ss_rate * @income  # 6.5%
    # now apply income tax
    case
    when @income < 15000
      case @no_of_children
      when 0
        tax(0.05)  # 5%
      when 1
        tax(0.03) # 3%
      else
        tax(0.02) # 2%
      end
    when @income < 25000
      case @no_of_children
      when 0
        tax(0.10)  # 10%
      when 1
        tax(0.08) # 8%
      else
        tax(0.06) # 6%
      end
    when @income < 55000
      case @no_of_children
      when 0
        tax(0.15)  # 15%
      when 1
        tax(0.12) # 12%
      else
        tax(0.11) # 11%
      end
    when @income < 105000
      case @no_of_children
      when 0
        tax(0.25)  # 25%
      when 1
        tax(0.23) # 23%
      else
        tax(0.20) # 20%
      end
    when @income < 205000
      case @no_of_children
      when 0
        tax(0.28)  # 28%
      when 1
        tax(0.25) # 25%
      else
        tax(0.30) # 30%
      end
    when @income < 255000
      case @no_of_children
      when 0
        tax(0.30)  # 30%
      when 1
        tax(0.28) # 28%
      else
        tax(0.30) # 30%
      end
    when @income < 305000
      case @no_of_children
      when 0
        tax(0.35)  # 35%
      when 1
        tax(0.30) # 30%
      else
        tax(0.20) # 20%
      end
    when @income < 405000
      case @no_of_children
      when 0
        tax(0.40)  # 40%
      when 1
        tax(0.35) # 35%
      else
        tax(0.30) # 30%
      end
    else  # income >= 405,000
      tax(0.50)
    end
    # blind people earning less than 65,000 don't pay income tax
    if @blind and @income < 65000
      @rate = BigDecimal.new('0')
      @income_tax = BigDecimal.new('0')
    end

    # military personnel pay only 75% of both taxes
    if @military
      @rate = @rate * 0.75.to_d
      @income_tax = @income_tax * 0.75.to_d
      @ss_rate = @ss_rate * 0.75.to_d
      @social_security_tax = @social_security_tax * 0.75.to_d
    end

    # students pay only 70% of income tax
    if @student
      @rate = @rate * 0.70.to_d
      @income_tax = @income_tax * 0.70.to_d
    end
  end

  def tax(rate)
    # puts "Taxing at: " + rate.to_s
    @rate = rate.to_d
    @income_tax = @income * rate.to_d
  end

  def income_tax
    @income_tax.truncate(2).to_s('10F')
  end

  def social_security_tax
    @social_security_tax.truncate(2).to_s('10F')
  end

  def discounts
    discounts = []
    if @blind and @income < 65000
      discounts << :blind
    end
    if @military
      discounts << :military
    end
    if @student
      discounts << :student
    end
  end

  def income
    @income
  end

  def no_of_children
    @no_of_children
  end

  def rate
    (@rate*100).truncate(2).to_s('3F')
  end

  def ss_rate
    (@ss_rate*100).truncate(2).to_s('3F')
  end
end
